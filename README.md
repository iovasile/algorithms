# Algorithms

The code I wrote while learning JS from online resources.
Only the solutions are available, check the exercises in the courses.
Organized by course.

Completed:

1. Javascript Algorithms And Data Structures Certification - freeCodeCamp
2. Javascript Basics for Beginners - https://www.udemy.com/javascript-basics-for-beginners/

Currently studying from:

1. Eloquent Javascript - 3rd Edition - https://eloquentjavascript.net/

After finishing it, I plan on updating it with katas from codewars.com.