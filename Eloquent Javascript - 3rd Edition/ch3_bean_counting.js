/*
Write a function countBs that takes a string as its only argument and returns a number that indicates how many uppercase “B” characters there are in the string.
*/

const countBs = str => {
  let foundBs = 0;
  for (let index = 0; index < str.length; index++) {
    str[index] === "B" ? foundBs++ : false;
  }
  return foundBs;
}

/*
Next, write a function called countChar that behaves like countBs, except it takes a second argument that indicates the character that is to be counted (rather than counting only uppercase “B” characters). Rewrite countBs to make use of this new function.
*/

const countChar = (str, char) {
  let foundChars = 0;
  for (let index = 0; index < str.length; index++) {
    str[index] === char ? foundCharss++ : false;
  }
  return foundChars;
}

/*
No idea how to rewrite countBs as of now
*/
