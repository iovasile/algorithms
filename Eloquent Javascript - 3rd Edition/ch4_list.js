/*
Write a function arrayToList that builds up a list structure like the one shown when given [1, 2, 3] as argument. nth, which takes a list and a number and returns the element at the given position in the list (with zero referring to the first element) or undefined when there is no such element.
*/

const arrayToList = arr => {
  let list = {}
  for (let i = arr.length - 1; i >= 0; i--) {
    list = {value: arr[i], rest: list};
  }
  return list;
}

// Also write a listToArray function that produces an array from a list.

const listToArray = list => {
  let arr = [];
  for (let node = list; node; node = node.rest) {
    arr.push(node.value);
  }
  return arr;
}

/* Then add a helper function prepend, which takes an element and a list and creates a new list that adds the element to the front of the input list
*/

const prepend = (elem, list) => {
  let newList = {};
  newList = {value: elem, rest: list};
  return newList;
}

/* Then add a helper function nth, which takes a list and a number and returns the element at the given position in the list (with zero referring to the first element) or undefined when there is no such element.
*/

const nth = (list, number) => {
  for (let node = list; node; node = node.rest) {
   	if (number == 0) {
      return node.value;
    } else {
      number--;
    }
  }
}
