/*
Write a function deepEqual that takes two values and returns true only if they are the same value or are objects with the same properties, where the values of the properties are equal when compared with a recursive call to deepEqual.
*/

const deepEqual = (x, y) => {
  if (x===y) {
    return true;
  } else if (typeof x != "object" && typeof x == null || typeof y != "object" && typeof y == null ) {
    return false;
  }
  let arrayX = Object.keys(x);
  let arrayY = Object.keys(y);
  if (arrayX.length != arrayY.length) {
    return false;
  }
  for (val of arrayX) {
    if (!arrayY.includes(val) || !deepEqual(x[val], y[val])) {
    return false;
    }
  }
  return true;
}
