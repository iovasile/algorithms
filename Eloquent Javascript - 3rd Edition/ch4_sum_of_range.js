/*
Write a range function that takes two arguments, start and end, and returns an array containing all the numbers from start up to (and including) end.
*/

const range = (start, end) => {
  let array = [];
  for(let i = start; i <= end; i++) {
    array.push(i);
  }
  return array;
}

/* Next, write a sum function that takes an array of numbers and returns the sum of these numbers. Run the example program and see whether it does indeed return 55.
*/

const sum = arr => {
  result = 0;
  for (let number of arr) {
    result += number;
  }
  return result;
}

/*As a bonus assignment, modify your range function to take an optional third argument that indicates the “step” value used when building the array. If no step is given, the elements go up by increments of one, corresponding to the old behavior.
*/

const modRange = (start, end, step) => {
  let array = [];
  if (step < 0) {
    for(let i = start; i >= end; i += step) {
      array.push(i);
    }
  } else if (step > 0) {
    for(let i = start; i <= end; i += step) {
      array.push(i);
    }
  } else {
    for(let i = start; i <= end; i++) {
      array.push(i);
    }
  }
  return array;
}
