/* Write a program that creates a string that represents an 8×8 grid, using newline characters to separate lines. At each position of the grid there is either a space or a "#" character. The characters should form a chessboard.

Passing this string to console.log should show something like this:

 # # # #
# # # #
 # # # #
# # # #
 # # # #
# # # #
 # # # #
# # # #
*/

let grid = "";
count = 0;
for (let column = 0; column < 8; column++) {
  if (column % 2 == 0) {
    for (let row = 0; row < 8; row++) {
      row % 2 == 0 ? grid += " " : grid += "#";
      if (row == 7) {
        break;
      }
    }
    grid = grid + "\n";
  } else if (column % 2 > 0) {
    for (let row = 0; row < 8; row++) {
      row % 2 == 0 ? grid += "#" : grid += " ";
      if (row == 7) {
        break;
      }
    }
    grid = grid + "\n";
  }
}
console.log(grid);

/* When you have a program that generates this pattern, define a binding size = 8 and change the program so that it works for any size, outputting a grid of the given width and height.
*/

const chessboard = size => {
  let grid = "";
  count = 0;
  for (let column = 0; column < size; column++) {
    if (column % 2 == 0) {
      for (let row = 0; row < size; row++) {
        row % 2 == 0 ? grid += " " : grid += "#";
        if (row == size-1) {
          break;
        }
      }
      grid = grid + "\n";
    } else if (column % 2 > 0) {
      for (let row = 0; row < size; row++) {
        row % 2 == 0 ? grid += "#" : grid += " ";
        if (row == size-1) {
          break;
        }
      }
      grid = grid + "\n";
    }
  }
  console.log(grid);
}

chessboard(12);
