// factory

function factoryAddress(street, city, zip) {
  return {
    street,
    city,
    zip
  }
}

// declare with factory

const myAddress = factoryAddress('a', 'b', 2);

// constructor

function ConstructorAddress(street, city, zip) {
  this.street = street;
  this.city = city;
  this.zip = zip;
}

// declare with constructor

const my2ndAddress = new ConstructorAddress('a', 'b', 2);