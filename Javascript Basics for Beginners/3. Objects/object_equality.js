const areEqual = (address1, address2) => {
  for (let key in address1)
    if (address1[key] !== address2[key])
      return false;
  return true;
};

const areSame = (address1, address2) => (address1 === address2);