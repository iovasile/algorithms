const address = {
  street: 'a',
  city: 'b',
  zip: 1
};

const showAddress = (address) => {
  for (let key in address)
    console.log(`${key}: ${address[key]}`);
};