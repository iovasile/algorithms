const priceRange = [
  {tier: $, name: inexpensive, minPrice: 1, maxPrice: 19},
  {tier: $$, name: moderate, minPrice: 20, maxPrice: 49},
  {tier: $$$, name: expensive, minPrice: 50, maxPrice: 100}
]