// simple implementation

const sum = (...num) => {
  return num.reduce((a, b) => a + b);
};

// able to receive array

const sum = (...num) => {
  if (Array.isArray(num[0]))
    return num[0].reduce((a, b) => a + b);
  return num.reduce((a, b) => a + b);
};

// using conditional operator (harder to read)

const sum = (...num) => Array.isArray(num[0]) ? num[0].reduce((a, b) => a + b) : num.reduce((a, b) => a + b);

// able to receive array like video

const sum = (...num) => {
  if (Array.isArray(num[0]))
    num = [...num[0]];
  return num.reduce((a, b) => a + b);
};