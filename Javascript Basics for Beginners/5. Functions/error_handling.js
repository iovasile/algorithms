try {
  const numbers = [1,2,3,4,1];
  const count = countOccurences(numbers, 1);
  console.log(count);
}
catch(e) {
  console.error(e);
}

function countOccurences(array, elem) {
  if (!Array.isArray(array))
    throw new Error('Not an Array!');
  return array.reduce((acc, curr) => {
    const occ = (curr === elem) ? 1 : 0;
    return acc + occ;
  }, 0);
}