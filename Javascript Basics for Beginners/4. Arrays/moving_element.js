const move = (array, index, offset) => {
  const newArr = [...array];
  const splicedValue = newArr.splice(index, 1);
  const moveTo = index + offset;

  
  if (moveTo < array.length && moveTo >= 0) {
    newArr.splice(index + offset, 0, splicedValue[0]);
    return newArr;
  }
  
  console.error('Invalid offset.')
};