const includes = (array, searchElement) => {
  for (let value of array)
    if (value === searchElement)
      return true;
      
  return false;
}