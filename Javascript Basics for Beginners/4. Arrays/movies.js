const sortMovies = (array, year, minRating) => {
  array.sort((a, b) => a.rating < b.rating)
  return array.forEach(obj => {
    if (obj.year === year && obj.rating > minRating)
      console.log(obj.title);
  });
};

// only methods

const answer = movies
  .filter(m => m.year === 2018 && m.rating >= 4)
  .sort((a, b) => a.rating < b.rating)
  .map(m => m.title);