const except = (array, excl) => {
  const output = [];

  for (let value of array)
    if (!excl.includes(value))
      output.push(value);
  
  return output;
}