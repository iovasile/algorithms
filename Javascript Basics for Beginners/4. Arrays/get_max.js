// simple implementation

const getMax = array => {
  array.sort((a, b) => b > a);
  return array[0];
};

// implementation using reduce

const reduceMax = array => {
  if (array.length === 0) return undefined;

  return array.reduce((a, b) => (a > b) ? a : b);
}