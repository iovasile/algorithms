// simple implementation

const countOccurences = (array, elem) => {
  let count = 0;

  array.forEach(n => {
    if (n === elem)
      count++;
  });

  return count;
};

// REDUCE implementation

const reduceOccurences = (array, elem) => {
  return array.reduce((acc, curr) => {
    const increment = (curr === elem) ? 1 : 0;
    return acc + increment;
  }, 0);
};