const checkSpeed = speed => {
  const limit = 70;
  const penalty = 5;

  if (speed < (limit + penalty))
    return 'Ok';

  const points = Math.floor((speed-limit)/penalty);
  
  return (points > 11) ? 'License Suspended' : `Points: ${points}`;
}