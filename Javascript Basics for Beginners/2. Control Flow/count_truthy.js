const countTruthy = array => {
  let count = 0;
  array.forEach(v => {
    if (v) 
      count++;
  });
  return count;
};