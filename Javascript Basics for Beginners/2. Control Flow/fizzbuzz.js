const fizzBuzz = input => {
  if (typeof input !== 'number')
    return NaN;
  if ((input % 5 === 0 ) && (input % 3 === 0))
    return 'fizzBuzz';
  if (input % 5 === 0)
    return 'buzz';
  if (input % 5 === 0)
    return 'fizz';
  return input;
}