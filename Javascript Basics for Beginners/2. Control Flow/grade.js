const calculateGrade = array => {
  const average = array.reduce((a, b) => a+b, 0) / array.length;
  if (average > 89) return 'A';
  if (average > 79) return 'B';
  if (average > 69) return 'C';
  if (average > 59) return 'D';
  return 'F';
};