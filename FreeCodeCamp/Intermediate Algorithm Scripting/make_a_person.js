/*
The methods that take an argument must accept only one argument and it has to be a string.

These methods must be the only available means of interacting with the object.
*/

const Person = function(firstAndLast) {
  let fullNameArray = firstAndLast.split(' ');
  this.setFullName = fullName => {
    if (typeof fullName != 'string') {
      return 'Argument must be a string';
    }
    fullNameArray = fullName.split(' ');
  };
  this.setFirstName = firstName => {
    if (typeof firstName != 'string') {
      return 'Argument must be a string';
    }
    fullNameArray[0] = firstName;
  };
  this.setLastName = lastName => {
    if (typeof lastName != 'string') {
      return 'Argument must be a string';
    }
    fullNameArray[1] = lastName;
  };
  this.getFullName = function() {
    return fullNameArray.join(' ');
  };
  this.getFirstName = function() {
    return fullNameArray[0];
  };
  this.getLastName = function() {
    return fullNameArray[1];
  };
};
