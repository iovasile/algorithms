/*
Return a new array that transforms the elements' average altitude into their orbital periods (in seconds).

The array will contain objects in the format {name: 'name', avgAlt: avgAlt}.

You can read about orbital periods on Wikipedia.

The values should be rounded to the nearest whole number. The body being orbited is Earth.

The radius of the earth is 6367.4447 kilometers, and the GM value of earth is 398600.4418 km3s-2.
*/

function orbitalPeriod(arr) {
  const GM = 398600.4418;
  let arr2 = [];
  arr.forEach(obj => {
    let a = 6367.4447; // Earth Radius
    a += obj.avgAlt;
    let orbitalPeriodEq = Math.round(2 * Math.PI * Math.sqrt(Math.pow(a, 3)/GM));
    let myObj = {
      name: obj.name,
      orbitalPeriod: orbitalPeriodEq
    };
    arr2.push(myObj);
  })
  return arr2;
}
