// Flatten a nested array. You must account for varying levels of nesting.

function steamrollArray(arr) {
  // I'm a steamroller, baby
  return arr.reduce((flat, content) => {
    return flat.concat(Array.isArray(content) ? steamrollArray(content) : content);
  }, []);
}
