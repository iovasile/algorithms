/*
Find the missing letter in the passed letter range and return it.

If all letters are present in the range, return undefined.
*/

function fearNotLetter(str) {
  let alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j','k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
  for (let i = 0; i < str.length; i++) {
    let index = alphabet.indexOf(str[i]);
    if (str[i+1] != alphabet[index+1]) {
      return alphabet[index+1];
    }
  }
  return undefined;
}
