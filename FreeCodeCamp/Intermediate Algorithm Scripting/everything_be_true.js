/*
Check if the predicate (second argument) is truthy on all elements of a collection (first argument).

In other words, you are given an array collection of objects. The predicate pre will be an object property and you need to return true if its value is truthy. Otherwise, return false.

In JavaScript, truthy values are values that translate to true when evaluated in a Boolean context.

Remember, you can access object properties through either dot notation or [] notation.
*/

// First try:

function truthCheck(collection, pre) {
  // Is everyone being true?
  let falsy = [false, null, undefined, 0, NaN, '', ""]
  let counter = 0;
  for (let obj of collection) {
    !falsy.includes(obj[pre]) ? counter++ : false;
  }
  return counter == collection.length ? true : false;
}

// Revision #1:
const truthCheck = (collection, pre) => {
  // Is everyone being true?
  let counter = 0;
  for (let obj of collection) {
    Boolean(obj[pre]) ? counter++ : false;
  }
  return counter == collection.length;
}
