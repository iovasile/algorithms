/*
Translate the provided string to pig latin.

Pig Latin takes the first consonant (or consonant cluster) of an English word, moves it to the end of the word and suffixes an "ay".

If a word begins with a vowel you just add "way" to the end.

Input strings are guaranteed to be English words in all lowercase.
*/

function translatePigLatin(str) {
  let arrayedStr = str.split('');
  if (arrayedStr[0].match(/[aeiou]/)) {
    arrayedStr.push('w');
  } else {
    for (let i = 0; i < arrayedStr.length; i++) {
      if (arrayedStr[0].match(/[^aeiou]/)) {
        arrayedStr.push(arrayedStr[0]);
        arrayedStr.shift();
      }
    }
  }
  arrayedStr.push('a', 'y')
  return arrayedStr.join('');
}
