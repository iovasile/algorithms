/*
Find the smallest common multiple of the provided parameters that can be evenly divided by both, as well as by all sequential numbers in the range between these parameters.

The range will be an array of two numbers that will not necessarily be in numerical order.

For example, if given 1 and 3, find the smallest common multiple of both 1 and 3 that is also evenly divisible by all numbers between 1 and 3. The answer here would be 6.
*/

function smallestCommons(arr) {
  arr.sort((a, b) => b-a);
  let newArr = [];
  for (let i = arr[0]; i >= arr[1]; i--) {
    newArr.push(i);
  }
  let count = 0;
  let times = 1;
  let x;
  do {
    count = newArr[0] * times * newArr[1];
    for (x = 2; x < newArr.length; x++) {
      if (count % newArr[x] !== 0) {
        break;
      }
    } times++;
  } while (x !== newArr.length);
  return count;
}
