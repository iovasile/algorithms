/*
Return an English translated sentence of the passed binary string.

The binary string will be space separated.
*/

const binaryAgent = str => {
  let stringArray = str.split(' ');
  let newArray = [];
  stringArray.forEach(val => newArray.push(String.fromCharCode(parseInt(val, 2))));
  return newArray.join('');
}
