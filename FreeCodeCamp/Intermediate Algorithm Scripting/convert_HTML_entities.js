/*
Convert the characters &, <, >, " (double quote), and ' (apostrophe), in a string to their corresponding HTML entities.
*/

function convertHTML(str) {
  // &colon;&rpar;
  let text = ['&', '<', '>', '"', "'"];
  let html = ['&amp;', '&​lt;', '&​gt;', '&​quot;', '&​apos;']
  let conversion = [];
  for (let ch in str) {
    let index = text.indexOf(str[ch]);
    if (index > -1) {
      conversion.push(html[index]);
    } else {
      conversion.push(str[ch]);
    }
  }
  return conversion.join('');
}
