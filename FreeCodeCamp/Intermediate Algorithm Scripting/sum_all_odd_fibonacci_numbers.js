/*
Given a positive integer num, return the sum of all odd Fibonacci numbers that are less than or equal to num.

The first two numbers in the Fibonacci sequence are 1 and 1. Every additional number in the sequence is the sum of the two previous numbers. The first six numbers of the Fibonacci sequence are 1, 1, 2, 3, 5 and 8.

For example, sumFibs(10) should return 10 because all odd Fibonacci numbers less than or equal to 10 are 1, 1, 3, and 5.
*/

const sumFibs = (num) => {
  let num0 = 0;
  let num1 = 1;
  let result = 0;
  while (num0 <= num) {
    if (num0 % 2 !== 0) {
      result += num0;
    }
    num0 += num1;
    num1 = num0 - num1;
  } return result;
}

// Another take

const sumFibs = (num) => {
  let fibArr = [1, 1]
  for(let i=fibArr.length; i<=num; i++) {
    fibArr[i] = fibArr[i-2] + fibArr[i-1];
  }
  let filteredArr = fibArr.filter(num => num % 2 == 1)
  let sumOddFibNum = 0;
  for (let i = 0; i < filteredArr.length; i++) {
    if (filteredArr[i] <= num)
      sumOddFibNum += filteredArr[i];
  }
  return sumOddFibNum;
}
