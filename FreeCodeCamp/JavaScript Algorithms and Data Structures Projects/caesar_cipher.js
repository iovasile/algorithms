/*
Write a function which takes a ROT13 encoded string as input and returns a decoded string.

All letters will be uppercase. Do not transform any non-alphabetic character (i.e. spaces, punctuation), but do pass them on.
*/

const rot13 = str => {
  let amArr = ['A', "B", "C", "D", 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];
  let nzArr = ['N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  let newArr = [];
  for (let i = 0; i < str.length; i++) {
    if (amArr.indexOf(str[i]) >= 0) {
      newArr.push(nzArr[amArr.indexOf(str[i])]);
    } else if (nzArr.indexOf(str[i]) >= 0) {
      newArr.push(amArr[nzArr.indexOf(str[i])]);
    } else if (amArr.indexOf(str[i]) < 0 && nzArr.indexOf(str[i]) < 0) {
      newArr.push(str[i]);
    }
  }
  return newArr.join('');
}
