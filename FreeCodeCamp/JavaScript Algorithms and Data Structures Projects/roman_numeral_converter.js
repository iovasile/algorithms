// Convert the given number into a roman numeral.

function convertToRoman(num) {
    let roman = [ 'M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I' ];
    let int = [ 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 ];
    let result = '';
    for(let i=0;i<int.length;i++) {
        while(int[i] <= num) {
            result+=roman[i];
            num-=int[i]
        }
    }
    return result;
}
