// Return true if the given string is a palindrome. Otherwise, return false.

const palindrome = str => {
  let newStr = str.replace(/[\W_]/g, '').toLowerCase();
  let j = newStr.length - 1;
  for (let i = 0; i < j/2; i++) {
    if (newStr[i] != newStr[j-i]) {
      return false;
    }
  }
  return true;
}
