/*
This was a hard one for me. I usually get a hard problem in a max of 2 days, but this time I struggled for around 4. I realised my time would be well spent developing the skills I lacked to resolve the problem, so I got the explanation from the hint page and I went through it until I was sure I understood what was going on.

For now, I still think my problem is not my understanding of the javascript concepts thats holding me back, but the way I approach the algorithm in the first place. This is something I will probably struggle with for some time, but maybe the next couple of courses I'll take to strengthen my knowledge will fix it. Anyways, it's been a fun journey, I'll probably come back to freeCodeCamp to get all my certifications. Hopefully I won't need them, but it will be something fun to do.
*/
